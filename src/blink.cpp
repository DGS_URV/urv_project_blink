#include "blink.h"


#include <dlib/opencv.h>
#include <opencv2/highgui/highgui.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <time.h>

using namespace dlib;
using namespace std;


blink::blink()
{
	win;

	// Load face detection and pose estimation models.//
	detector = get_frontal_face_detector();
	pose_model;
	deserialize("shape_predictor_68_face_landmarks.dat") >> pose_model;

	// Grab and process frames until the main window is closed by the user.
	calibrating_timer = clock(); //timer para calibrar EAR


	//Attributes
	EAR_izq, EAR_der;
	start_timer = 0, calibrating_timer = 0;
	threshold = 0;

	accion = false, cerrado = false, calibrated = false;

	EAR_der_max = 0;
	EAR_der_min = 0;
	EAR_izq_max = 0;
	EAR_izq_min = 0;

	remaining_time = 0;

}


blink::~blink()
{
}


float blink::calculateEAR(point p1, point p2, point p3, point p4, point p5, point p6)
{
	float EAR;
	float unit1, unit2, unitario, determinante;

	unit1 = Distance(p2.x(), p2.y(), p6.x(), p6.y());
	unit2 = Distance(p3.x(), p3.y(), p5.x(), p5.y());

	unitario = unit1 + unit2;

	determinante = 2 * Distance(p1.x(), p1.y(), p4.x(), p4.y());

	EAR = unitario / determinante;

	return EAR;
}

double blink::Distance(double dX0, double dY0, double dX1, double dY1) {
	return sqrt((dX1 - dX0) * (dX1 - dX0) + (dY1 - dY0) * (dY1 - dY0));
}

float blink::calibrate_threshold(float EAR_der, float EAR_izq) {

	/*float EAR_der_max = 0;
	float EAR_der_min = 0;
	float EAR_izq_max = 0;
	float EAR_izq_min = 0;*/
	float EAR;
	float EAR_izq_temp, EAR_der_temp;

	remaining_time = (clock() - calibrating_timer) / CLOCKS_PER_SEC;

	if (remaining_time > 5)
	{
		calibrated = true;

		//Calcular threshold derecha al 70%
		EAR_der_temp = EAR_der_max - EAR_der_min;
		EAR_der_temp = (EAR_der_temp * 0.7) + EAR_der_min;

		//Calcular threshold izquierda al 70%
		EAR_izq_temp = EAR_izq_max - EAR_izq_min;
		EAR_izq_temp = (EAR_der_temp * 0.7) + EAR_izq_min;

		//Calcular media
		EAR = (EAR_der_temp + EAR_izq_temp) / 2;



	}
	else
	{
		if (EAR_der_max < EAR_der)
			EAR_der_max = EAR_der;

		else if (EAR_der_min > EAR_der)
			EAR_der_min = EAR_der;

		else if (EAR_izq_max < EAR_izq)
			EAR_izq_max = EAR_izq;

		else if (EAR_izq_min > EAR_izq)
			EAR_izq_min = EAR_izq;
	}


	return EAR;

}

bool blink::checkClosed(float eye_threshold, int seconds_to_go, float EAR_izq, float EAR_der) {

	//float EAR = (EAR_izq + EAR_der) / 2;

	if ((EAR_der > eye_threshold) || (EAR_izq > eye_threshold)) {

		cerrado = false;
		accion = false;
		//	cout << "EAR: " << EAR_der << " Abierto \n";
	}
	else if (((EAR_der <= eye_threshold) || (EAR_izq <= eye_threshold)) && (!cerrado)) {
		cerrado = true;

		cout << "EAR: " << EAR_der << " Cerrado...ESPERANDO \n";
		start_timer = clock();
	}
	else if (cerrado) {

		float tiempo_pasado = (clock() - start_timer) / CLOCKS_PER_SEC;

		//cout << "tIEMPO PASADO: " << tiempo_pasado << "  \n";
		//cout << "CERRADOOOOO: " << "  \n";
		if ((!accion) && (tiempo_pasado > seconds_to_go)) {
			//cout << "CERRADOOOOO: " << "  \n";
			accion = true;

		}

	}
	return accion;

}


bool blink::check_blinking(cv::Mat temp) {

	//cv::Mat temp;
	//cap >> temp;
	// Turn OpenCV's Mat into something dlib can deal with.  Note that this just
	// wraps the Mat object, it doesn't copy anything.  So cimg is only valid as
	// long as temp is valid.  Also don't do anything to temp that would cause it
	// to reallocate the memory which stores the image as that will make cimg
	// contain dangling pointers.  This basically means you shouldn't modify temp
	// while using cimg.
	cv_image<bgr_pixel> cimg(temp);

	// Detect faces 
	std::vector<rectangle> faces = detector(cimg);
	// Find the pose of each face.
	std::vector<full_object_detection> shapes;
	for (unsigned long i = 0; i < faces.size(); ++i) {


		shapes.push_back(pose_model(cimg, faces[i]));

		//Coordenadas ojo derecho. Landmarks 36 a 41
		point p1 = pose_model(cimg, faces[i]).part(36); //p1; int p1x = p1.x; int p1y = p1.y;
		point p2 = pose_model(cimg, faces[i]).part(37); //p2
		point p3 = pose_model(cimg, faces[i]).part(38); //p3
		point p4 = pose_model(cimg, faces[i]).part(39); //p4
		point p5 = pose_model(cimg, faces[i]).part(40); //p5
		point p6 = pose_model(cimg, faces[i]).part(41); //p6

		//Coordenadas ojo izquierdo. Landmarks 42 a 47
		point p7 = pose_model(cimg, faces[i]).part(42); //p1															
		point p8 = pose_model(cimg, faces[i]).part(43); //p2
		point p9 = pose_model(cimg, faces[i]).part(44); //p3
		point p10 = pose_model(cimg, faces[i]).part(45); //p4
		point p11 = pose_model(cimg, faces[i]).part(46); //p5
		point p12 = pose_model(cimg, faces[i]).part(47); //p6



		EAR_der = calculateEAR(p1, p2, p3, p4, p5, p6);
		EAR_izq = calculateEAR(p7, p8, p9, p10, p11, p12);

		if (!calibrated)
			threshold = calibrate_threshold(EAR_der, EAR_izq);
		else
			checkClosed(threshold, 2, EAR_izq, EAR_der); //threshold and seconds to count until click



	}



	// Display it all on the screen
	win.clear_overlay();
	win.set_image(cimg);
	win.add_overlay(render_face_detections(shapes));

	return accion;
}