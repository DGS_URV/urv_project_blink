#pragma once


#include <dlib/opencv.h>
#include <opencv2/highgui/highgui.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <time.h>

class blink
{
public:
	blink();
	~blink();


	dlib::image_window win;

	// Load face detection and pose estimation models.//
	dlib::frontal_face_detector detector = dlib::get_frontal_face_detector();
	dlib::shape_predictor pose_model;
	//deserialize("shape_predictor_68_face_landmarks.dat") >> pose_model;

	//Attributes
	float  EAR_izq, EAR_der;
	clock_t start_timer = 0, calibrating_timer = 0;
	float threshold = 0;
	bool accion = false, cerrado = false, calibrated = false;

	float EAR_der_max = 0;
	float EAR_der_min = 0;
	float EAR_izq_max = 0;
	float EAR_izq_min = 0;

	float remaining_time;

	//Methods
	double Distance(double dX0, double dY0, double dX1, double dY1);
	float calibrate_threshold(float EAR_der, float EAR_izq);
	float calculateEAR(dlib::point p1, dlib::point p2, dlib::point p3, dlib::point p4, dlib::point p5, dlib::point p6);
	bool checkClosed(float eye_threshold, int seconds_to_go, float EAR_izq, float EAR_der);
	bool check_blinking(cv::Mat cap);

	 
};

